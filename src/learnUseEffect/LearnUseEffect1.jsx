import React, { useEffect, useState } from 'react'

const LearnUseEffect1 = () => {

    let [count, setCount] = useState(0)
    let [count1, setCount1] = useState(100)

// ---> useEffect(fun) (without dependency)
// ---> useEffect will run without dependency.

// ---> useEffect (fun, dependency)
// ---> useEffect (fun, []) // it will only run on first render
// ---> useEffect (fun, [count1]) // it will run for first render and depends upon dependency for second render
// ---> useEffect (fun, [count1,count2]) // it will run for first render and depends upon count1 and count2 dependency for second render


    // useEffect(()=>{
    //     console.log('I am useEffect')
    // }, [])



    
  return (
    <div>
    <br></br>
        LearnUseEffect1
    <br></br>

    count is {count}
    <br></br>
    count is {count1}
    <br></br>

    <button onClick={()=>{
        setCount(count+1)
    }}
    >
        Increment of 1
    </button>
    <br></br>

    <button onClick={()=>{
        setCount1(count1+1)
    }}
    >
        Increment of 100
    </button>

</div>
  )
}

export default LearnUseEffect1