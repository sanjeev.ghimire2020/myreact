import React from 'react'

const TernaryOperator = () => {

    let age = 25
  return <div>
    {
        age>=18? <div>S/he can enter the room</div>
        :<div>S/he can't enter the room</div>
  }
  </div>
}

export default TernaryOperator