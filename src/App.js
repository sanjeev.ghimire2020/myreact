import React, { useState } from 'react'
import InlineCss from './InlineCss'
import TernaryOperator from './learnComponent/TernaryOperator'
import LearnUseState from './LearnUseState/LearnUseState'
import LearnUseState1 from './LearnUseState/LearnUseState1'
import LearnUseState3 from './LearnUseState/LearnUseState3'
import LearnUseState4 from './LearnUseState/LearnUseState4'
import LearnUseState6 from './LearnUseState/LearnUseState6'
import LearnUseStateCount from './LearnUseState/LearnUseStateCount'
import LearnUseState7 from './LearnUseState/LearnUseState7'
import LearnUseState8Practice from './LearnUseState/LearnUseState8Practice'
import LearnUseStatePractice from './LearnUseState/LearnUseStatePractice'
import ReducerPractice from './LearnReducer.jsx/ReducerPractice'
import LearnUseStatePracice2 from './LearnUseState/LearnUseStatePracice2'
import LearnUseState9 from './LearnUseState/LearnUseState9'
import LearnUseEffect1 from './learnUseEffect/LearnUseEffect1'
import LearnCleanUpFunction2 from './learnUseEffect/LearnCleanUpFunction2'
import Form1 from './Form/Form1'

// const App = () => {

//   let [show,setShow] = useState(true)
//   return (
//     <div>
      {/* <InlineCss></InlineCss>
      {/* <TernaryOperator></TernaryOperator> */}
      {/* <LearnUseState></LearnUseState> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState3></LearnUseState3> */}
      {/* <LearnUseState4></LearnUseState4> */}
      {/* <LearnUseState6></LearnUseState6> */}
      {/* {<LearnUseStateCount></LearnUseStateCount>} */}
      {/* {<LearnUseState7></LearnUseState7>} */}
      {/* <LearnUseState8Practice></LearnUseState8Practice> */}
      {/* <LearnUseStatePractice></LearnUseStatePractice> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <ReducerPractice></ReducerPractice> */}
      {/* {<LearnUseStatePracice2></LearnUseStatePracice2>} */}
      {/* {<LearnUseState9></LearnUseState9>} */}
      {/* {<LearnUseEffect1></LearnUseEffect1>} */}
      {<Form1></Form1>}

      
{/* 
      {show? <LearnCleanUpFunction2></LearnCleanUpFunction2> :null}

      <button
      onClick={()=>{
        setShow(false)
      }}
      >
        Hide Component
      </button>
    </div>
  )
}

export default App */}