import React, { useState } from 'react'

const LearnUseStatePractice = () => {
    let [count,setCount] = useState(0)
  return (
    <div>
        Count is {count}
        <br></br>
        <button onClick={()=>{setCount(count+1)}}>Increment</button>
        <br></br>
        <button onClick={()=>{setCount(count-1)}}>Decrement</button>
        <br></br>
        <button onClick={()=>{setCount(0)}}>Reset</button>
    </div>
  )
}

export default LearnUseStatePractice

//-----------###########-------------##########----------------------->>>>>

// import React, {useState} from 'react'

// function LearnUseStatePractice() {
//     const [count, setCount] = useState(4)

//     function decrementCount() {
//         setCount(prevCount => prevCount -1)
//     }

//     function incrementCount(){
//         setCount(prevCount => prevCount +1)
//     }

//   return (
//     <>
//     <button onClick={decrementCount}>-</button>
//     <span>{count}</span>
//     <button onClick= {incrementCount}>+</button>
//     </>
//   )
// }

// export default LearnUseStatePractice

//----------##########-----------$$$$$$$$$$$$$----------------->>>>>>>>

// import React, { useState } from 'react'

// const LearnUseStatePractice = () => {
//     const [inputValue, setInputValue] = useState('Sanjeev')
//     let onChange = (event) => {
//         const newValue = event.target.value
//         setInputValue(newValue)
//     }    
//   return (
//     <div>
//         <input placeholder='Enter Something' onChange={onChange}/>
//         {inputValue}
//     </div>
//   )
// }

// export default LearnUseStatePractice