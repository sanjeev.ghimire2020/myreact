import React, { useState } from 'react'

const LearnUseState6 = () => {
  let [name,setName]= useState('Sanjeev')
  return (
    <div>
      name is {name}
    <br></br>
    <button 
      onClick={()=>{
        setName('Shaailendra')
    }}> Change Name </button>

    </div>
  )
}

export default LearnUseState6