import React, {useState} from 'react'

const LearnUseState7 = () => {
  let [count, setCount] = useState(0)
  console.log('Hello')
  let [count1, setCount1] = useState(100)
  return (
    <div>
         Count is {count}
      <br></br>
        Count1 is {count1}
        <br></br>
      <button 
      onClick={()=>{
        setCount(count+1)
      }}
      >
       Increment
      </button>
      <br></br>
      <button onClick={()=>{
        setCount1(count1+1)
      }}
      >
        Increment button
      </button>
    </div>
  )
}

export default LearnUseState7


//When a component is rendeered, the state variable will be updated if the component is rendeered by that variable else state variable nwill bholed ther previosus value.


//if any state variable is updated, the page will be executed such that the state variable which is updated holds the updated value and other state variables hold the previous value.

//Note it is necessary a component will render with setName, setAge, setCount or set... is called.