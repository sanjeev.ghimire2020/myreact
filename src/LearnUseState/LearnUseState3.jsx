import React, { useState } from 'react'

const LearnUseState3 = () => {
    let [showError,show] = useState(true)
    return (
        <div>
            {showError?<div style={{ color: 'red' }}>This is Error</div> : null}
            <br></br>
            <button
            onClick={()=>{
                show(true)
            }}
            >
            to show
            </button>
            <br></br>
            <button
            onClick={()=>{
                show(false)
            }}
            >
            to hide
            </button>
            
        </div>
    )
}

export default LearnUseState3